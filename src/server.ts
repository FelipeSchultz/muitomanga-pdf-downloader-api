import 'dotenv/config'
import Application from './config/Application';

const { PORT, SERVER_PORT } = process.env;

const port = PORT || SERVER_PORT || 3000

Application.listen(port, () => {
    console.log(`Server started on port: ${port}`);
})
