import { Router } from "express";
import { convertToPdf } from "../services/ConverterService";

const converterRoutes = Router();

converterRoutes.post('/pdf', convertToPdf)
converterRoutes.get('/teste', (_, res) => res.json({ status: "success" }))

export { converterRoutes }
