import { Request, Response } from "express";
import PDFDocument from 'pdfkit'
import https from 'https'
import imageSize from 'buffer-image-size'

export const convertToPdf = async (req: Request, res: Response) => {
    const { name, images } = req.body;

    try {
        const doc = new PDFDocument({
            autoFirstPage: false, info: {
                Title: name,
                Author: 'MuitoManga Downloader',
                Producer: 'MuitoManga Downloader'
            }
        })

        res.set({
            'Content-type': 'application/pdf',
            'Content-disposition': `attachment; filename=${name}.pdf`
        })

        doc.pipe(res)

        for (const img of images) {
            const image = await getImageBuffer(img);

            const { width, height } = imageSize(image);

            doc.addPage({
                size: [width, height]
            })

            doc.image(image, 0, 0, {
                width,
                height
            })
        }

        doc.end();
    } catch (error) {
        return res.status(500).json({
            error: {
                message: 'Error on create PDF',
                stackTracre: error
            }
        })
    }
}

const getImageBuffer = async (url: string): Promise<Buffer> => {
    return new Promise((resolve, reject) => {
        https.get(url, (res) => {
            const data = [];
            res.on('data', (chunk) => {
                data.push(chunk);
            }).on('end', () => {
                let buffer = Buffer.concat(data);

                resolve(buffer)
            });
        }).on('error', (err) => {
            reject(err)
        });
    })
}