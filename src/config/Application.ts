import Express, { Express as IExpress } from 'express'
import bodyParser from 'body-parser'
import cors from 'cors'

import { converterRoutes } from '../routes/Converter.routes';

class Application {
    express: IExpress;

    constructor() {
        this.express = Express();

        this.middlewares()
        this.routes()
    }

    private middlewares() {
        this.express.use(cors({ origin: 'https://muitomanga.com', exposedHeaders: '*', methods: '*' }))
        this.express.use(bodyParser.json())
        this.express.use(Express.json())
    }

    private routes() {
        this.express.use('/convert', converterRoutes)
    }
}

export default new Application().express
